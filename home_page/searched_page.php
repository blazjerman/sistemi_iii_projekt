<?php

    session_start(); 
    include("../DB/db.php");
    include("../time/time.php");
    include("../errors/check_if_loogin.php");

    $sql = "SELECT * FROM prijava WHERE (ime REGEXP '.*".$_SESSION["zadnjr_iskanje"].".*')";
    $prijava = mysqli_query($db,$sql);

?>
<!DOCTYPE html>
<html>
    <head>

        <link rel="stylesheet" href="../style/style.css?v=<?php echo time(); ?>">

        <title>Baza tel. st.</title>
        <style>#n1{background-color: #18191a;}</style>
    </head>
    <body style="background-color: #18191a;background-size: 100% ;">
        <?php
            include('../site_parts/header.php'); 
        ?>
        <div id="prostor_za_objave">
            <h1 style="width:1000px;margin-left:-170px;">Rezultati iskanja:</h1>
            <?php
                if ($prijava->num_rows > 0) {
                    while($vrstica = $prijava->fetch_assoc()){
                        
                        echo "<div class='post'>";
                        $id=$vrstica["id"];

                        echo "<a href='../report_page/report_page.php?id=".$id."'><h2>".$vrstica["ime"]."</h2></a>";
                        echo "<p style='color: #646464;' class='time'>".get_time_ago($vrstica["datum"])."</p>";

                        $sqlUporabnika = "SELECT uporabnisko_ime FROM uporabnik WHERE id='$vrstica[uporabnik_id]'";
                        $uporabnik = mysqli_query($db,$sqlUporabnika)->fetch_assoc();
                        
                        echo "<p style='color: #646464;height:10px;font-size:11px;width:100px;margin-top:-75px;margin-left:500px'>Objavil: ".$uporabnik["uporabnisko_ime"]."</p>";                    

                        $vseSike = $db->query("SELECT slika FROM slike WHERE prijava_id = {$vrstica['id']}");
    
                        if($vseSike->num_rows > 0){
                            while($slike = $vseSike->fetch_assoc()){
                                $slika = $slike['slika'];              
                                echo "<div class='pristorZaSliko'><img height='371px' style='  max-width: 660px;margin-left: auto;margin-right: auto;display: block;' src='data:image/jpeg;base64,".$slika."'/></div>";
                            }
                        }

                        $vsecekSql = "SELECT * FROM emocije WHERE vsecek=1 and prijava_id='$id'";
                        $vsecekRows = mysqli_query($db,$vsecekSql);
                        $vsecki = mysqli_num_rows($vsecekRows);

                        $sem_naletelSql = "SELECT * FROM emocije WHERE sem_naletel=1 and prijava_id='$id'";
                        $sem_naletelRows = mysqli_query($db,$sem_naletelSql);
                        $sem_naletel = mysqli_num_rows($sem_naletelRows);

                        $sqlKomentarji = "SELECT * FROM komentarji WHERE prijava_id='$id'";
                        $komentarji = mysqli_query($db,$sqlKomentarji);

                        echo "<p>".$vrstica["opis"]."</p><hr>";
                        echo "<div class='gumbL'><a href='../report_page/report_page.php?id=".$id."'><input type='submit' value='Komentiraj ".$komentarji->num_rows."'/></a></div>";
                        echo "<div class='gumbL'><a href='../emotions/like.php?id=".$id."'><input type='submit' value='Všeč mi je ".$vsecki."'/></a></div>";
                        echo "<div class='gumbL'><a href='../emotions/seen.php?id=".$id."'><input type='submit' value='Sem že naletel na prevaro ".$sem_naletel."'/></a></div>";

                        if($vrstica["preverjeno"]==1){
                            echo "<img style='width: 35px;margin-left: 20px;' src='../pictures/correct.png' alt='Preverjeno'>";
                        }else{
                            echo "<br><br>";
                        }

                        echo "</div>";

                    }
                }else{
                    echo "<h2 style='text-align:center;'>Našli nismo ničesar.</h2>";
                }
            ?>
        </div>
        <footer>
    </body>
    </html>
<?php
?>



             