<!DOCTYPE html>
    
<html>
    <head>
        <title>Nastavitve računa</title>
        <link rel="stylesheet" href="../style/style.css">
        <style>#n2{background-color: #18191a;border-radius: 10px;}</style>
    </head>
    <body>

    <?php

        session_start();
        include("../DB/db.php");
        include("../errors/check_if_loogin.php");

        $id=$_SESSION["id_uporabnika"];
        $sql =  "SELECT * FROM uporabnik WHERE id = '$id'";
        $podatkiUporabnika = mysqli_query($db,$sql)->fetch_assoc();


        include('../site_parts/header.php');
    
        echo '<h1 style="width:700px;">Nastavitve računa</h1>';
        if($_GET['tmp']=="true"){
            echo '<h1 class="uPosodobljeno" style="width:800px;margin-bottom:-137px">Uspešno posodobljeno</h1>';
        }  
        echo'<div class="reg">
            <form action="edit.php" method="post">
        
            <fieldset>
                    <label>Uporabniški ime</label>
                    <input required minlength="1" maxlength="11" title="Min 1 max 11 znakov." type="text" name="uporabnisko_ime" placeholder="Uporabniško ime" value="'.$podatkiUporabnika["uporabnisko_ime"].'"><br>

                    <label>Ime</label>
                    <input required minlength="1" maxlength="11" title="Min 1 max 11 znakov." type="text" name="ime" placeholder="Ime" value="'.$podatkiUporabnika["ime"].'"><br>
                    <label>Priimek</label>
                    <input required minlength="1" maxlength="11" title="Min 1 max 11 znakov." type="text" name="priimek" placeholder="Priimek" value="'.$podatkiUporabnika["priimek"].'"><br>
                    <label>Naslov</label>
                    <input required minlength="1" maxlength="50" title="Min 1 max 50 znakov." type="text" name="naslov" placeholder="Naslov" value="'.$podatkiUporabnika["naslov"].'"><br>
                    <label>Telefonska številka</label>
                    <input required minlength="1" maxlength="11" pattern="\d{3,11}$" title="Min 3 max 11 znakov." type="text" name="telst" placeholder="Telefonska številka" value="'.$podatkiUporabnika["telst"].'"><br>
                    <label>E-pošta</label>
                    <input required minlength="1" maxlength="50" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="Vnesi pravilni email."  type="text" name="eposta" placeholder="E-pošta" value="'.$podatkiUporabnika["eposta"].'"><br>

                    
                    <label>Geslo</label>
                    <input required minlength="1" maxlength="11" type="password" name="geslo" placeholder="Geslo" value="'.$podatkiUporabnika["geslo"].'"><br> 
                    <label>Ponovi geslo</label>
                    <input required minlength="1" maxlength="11" type="password" name="geslo_0" placeholder="Ponovi geslo" value="'.$podatkiUporabnika["geslo"].'"><br> 
                </fieldset>
            <input type="submit" value="Posodobi podatke">
            </form> 
         </div>';
         ?>  
    </body>
</html>