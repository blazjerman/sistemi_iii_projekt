<!DOCTYPE html>
    
<html>
    <head>
        <title>O projektu</title>
        <link rel="stylesheet" href="../style/style.css?v=<?php echo time(); ?>">

        <style>#n10{background-color: #18191a;border-radius: 5px;}</style>

    </head>
    <body>
        <?php
            session_start();
            include('../site_parts/header.php');  
        ?>
        <div style="margin:auto;height:1300px">
            <h1 class="center">O projektu</h1>
                <p style="width=70%;display:flex;justify-content:center;">
                    Projekt je bil izdelan skupaj z seminarsko nalogo, ki jo vidimo spodaj.    
                </p>
            <div style="display:flex;justify-content:center;">
                <iframe  src="../download_files/seminarska.pdf" width="70%" height="1000px"></iframe>
            </div>
        </div>
        <?php
            include('../site_parts/footer.php');
        ?>
    </body>
</html>