<!DOCTYPE html>
<?php
    session_start();
    include("../errors/check_if_loogin.php");
?>
<html>
    <head>
        <title>Prijava</title>
        <link rel="stylesheet" href="../style/style.css">
    <style>
        #n7{background-color: #18191a;}
        #textArea{
            height:100px;
            width:100%;
        }
        
    </style>
    </head>
    <body>
        <?php
            session_start();
            include('../site_parts/header.php');     
        ?>
        <h1>Nova prijava</h1>
         <form class="reg" action="create.php" method="post" enctype="multipart/form-data">
            <fieldset>
                <label>Naslov</label>
                <input required minlength="1" maxlength="50"  title="Min 1 max 50 znakov." type="text" name="naslov" placeholder="Naslov"><br>
                <label>Opis</label>
                <textarea required minlength="1" maxlength="2047" title="Min 1 max 2047 znakov." id="textArea" id="textArea" rows="10"  name="opis" placeholder="Opis"></textarea><br> 
                <label>Izberi sliko</label>
                <input accept="image/jpg, image/png, image/gif, image/jpeg" type="file" name="slika" value="Naloži sliko">
            </fieldset>
            <input type="submit" value="Submit">
         </form>
    
    </body>
</html>