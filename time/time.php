<?php
function get_time_ago( $time )
{
    $time_difference = time() - strtotime($time);

    if( $time_difference < 1 ) { return 'Pred manj kot eno minuto.'; }
    $condition = array( 12 * 30 * 24 * 60 * 60 =>  'l',
                30 * 24 * 60 * 60       =>  'mes',
                24 * 60 * 60            =>  'd',
                60 * 60                 =>  'u',
                60                      =>  'min',
                1                       =>  'sek'
    );

    foreach( $condition as $secs => $str )
    {
        $d = $time_difference / $secs;

        if( $d >= 1 )
        {
            $t = round( $d );
            return 'Pred ' . $t . ' ' . $str . '';
        }
    }
}
?>