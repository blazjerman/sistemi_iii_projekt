<!DOCTYPE html>
    
<html>
    <head>
        <title>Registracija</title>
        <link rel="stylesheet" href="../style/style.css?v=<?php echo time(); ?>">
        <style>#n5{background-color: #18191a;}</style>
    </head>
    <body style="background-attachment: fixed;background-repeat: no-repeat;background-size: cover;background-color:black;margin: 0%;background-image: url('../pictures/tec2.png');background-size: 100% ;">

    <?php
        include('../site_parts/header.php')     
    ?>
    <h1>Registracija</h1>
    <div class="reg">
         <form action="register.php" method="post">
    
         <fieldset>
                <label>Uporabniško ime</label>
                <input required minlength="1" maxlength="11" title="Min 1 max 11 znakov." type="text" name="uporabnisko_ime" placeholder="Uporabniško ime"><br>

                <label>Ime</label>
                <input required minlength="1" maxlength="11" title="Min 1 max 11 znakov." type="text" name="ime" placeholder="Ime"><br>
                <label>Priimek</label>
                <input required minlength="1" maxlength="11" title="Min 1 max 11 znakov." type="text" name="priimek" placeholder="Priimek"><br>
                <label>Naslov</label>
                <input required minlength="1" maxlength="50" title="Min 1 max 50 znakov." type="text" name="naslov" placeholder="Naslov"><br>
                <label>Telefonska številka</label>
                <input required minlength="1" maxlength="11" pattern="\d{3,11}$" title="Min 3 max 11 znakov." type="text" name="telst" placeholder="Telefonska številka"><br>
                <label>E-pošta</label>
                <input required minlength="1" maxlength="50" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="Vnesi pravilni email." type="text" name="eposta" placeholder="E-pošta"><br>

                
                <label>Geslo</label>
                <input required minlength="1" maxlength="11" title="Min 1 max 11 znakov." type="password" name="geslo" placeholder="Geslo"><br> 
                <label>Ponovi geslo</label>
                <input required minlength="1" maxlength="11" title="Min 1 max 11 znakov." type="password" name="geslo_0" placeholder="Ponovi geslo"><br> 
                

<!--                <h2 Style="font-size:20px;margin-left:-16px;">Označite naprave, ki jih uporabljate:</h2>
                <div style="height:20px;display:flex">
                    <label for="mac">Mac:</label>
                    <input type="checkbox" name="naprave[]" value="mac">
                    <label for="android">Android:</label>
                    <input type="checkbox" name="naprave[]" value="android">
                    <label for="ios">IOS:</label>
                    <input type="checkbox" name="naprave[]" value="ios">
                    <label for="win">Windows:</label>
                    <input type="checkbox" name="naprave[]" value="win"> 
                    <label for="linux">Linux:</label>
                    <input type="checkbox" name="naprave[]" value="linux"> 
                </div><br>
-->
            </fieldset>
          <input type="submit" value="Registriraj se">
         </form> 
         </div>   
    </body>
</html>