<!DOCTYPE html>

<html>
    <head>
        <title>Dobrodošli</title>
        <link rel="stylesheet" href="../style/style.css?v=<?php echo time(); ?>">
        <style>#n4{background-color: #18191a;}</style>

    </head>
    <body id="dobrodosli">
    <?php
    include('../site_parts/header.php'); 
    ?>
    <div class="vsebina">
        <h1 class="center" style='width:1300px;'>Elektronske prevare</h1>
        <div class="opis">
            <div class="opisSlikaLevo opisSlika">
                <p>
                Zaradi napredka tehnologije vse več ljudi uporablja internet. V času 
                korone se je veliko storitev preselilo na splet in tako so se morali
                preseliti tudi njihovi uporabniki. Na spletu lahko dobimo veliko pametnih
                informacij in storitev, od socialnih omrežij, novic, trgovin…. Seveda pa
                poleg vseh dobrih stvari na internetu obstajajo tudi slabe. Vsak nov 
                uporabnik spleta bo prej ali slej naletel na neke vrste spletno prevaro.
                    Pa naj bo to klik na reklamo ali sumljiv sms link.
                </p>
            </div>
            <div class="opisSlikaDesno opisSlika">
                <p>
                Glaven namen aplikacije je prijavljanje in pregled spletnih 
                in podobnih prevar. Tako bi lahko uporabnik interneta pred klikom na 
                sumljivo spletno stran preveril, če je le ta v naši bazi.
                </p>
            </div>
        </div>
        <div class="opis_0">
            <div class="opisSlikaLevo opisSlika">
                <p>Platforma omogoča prijavo raznih internetnih,
                telefonskih in drugih elektronskih prevar, (scamov)
                poleg tega pa lahko uporabnik, ki je naletel na
                kakšno čudno spletno okoliščino to preveri, preden
                bi se je udeležil. Platforma ponuja tudi 
                avtomatsko preverjanje (kot nekakšen antivirus v obliki
                add-ona v brskalniku ali aplikaciji).
            </p>
            </div>
            <div class="opisSlikaLevo opisSlika slika">
                <img src="../pictures/bait.png" alt="Slika" srcset="">
            </div>
        </div>

        <div style="height:500px;" class="opis_0">
            <div class="opisSlikaDesno opisSlika slika">
                <img style="height:500px;width: auto;" src="../pictures/apple.png" alt="Slika" srcset="">
            </div>
            <h2 class="opisSlikaDesno" style="margin-right:250px;" >Zgled:</h2>
            <h2>Uporabljaš jo lahko že danes!</h2>
            <p>Registriraš se lahko s spodnjim gumbom:</p>
            <a style="float:left;margin-left:300px;" class=right id="n5" href="../register/register_page.php">Registracija</a> 
        </div>

        <div class="opis2">
        </div>
            
        </div>
    </div>
    <?php
        include('../site_parts/footer.php'); 
        ?>
    </body>
</html>